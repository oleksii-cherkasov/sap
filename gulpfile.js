'use strict';

var gulp = require('gulp'),
	watch = require('gulp-watch'),
	prefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	rigger = require('gulp-rigger'),
	cssmin = require('gulp-minify-css'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	rimraf = require('rimraf');

var path = {
	build: {
		js: 'js/',
		css: 'css/',
		img: 'images/'
	},
	src: {
		js: 'src/js/app.js',
		style: 'src/styles/app.scss',
		img: 'src/images/**/*.*'
	},
	watch: {
		js: 'src/js/app.js',
		style: [
			'src/styles/**/*.scss',
			'src/vendor/**/*.scss',
			'src/vendor/**/*.css'
			],
		img: 'src/images/**/*.*'
	}
};

gulp.task('js:build', function () {
	gulp.src(path.src.js) 
		.pipe(rigger()) 
		.pipe(sourcemaps.init()) 
		.pipe(uglify()) 
		.pipe(sourcemaps.write()) 
		.pipe(gulp.dest(path.build.js));
});

gulp.task('styles:build', function () {
	gulp.src(path.src.style) 
		.pipe(sourcemaps.init())
		.pipe(sass({
			includePaths: ['src/styles/'],
			outputStyle: 'compressed',
			sourceMap: true,
			errLogToConsole: true
		}))
		.pipe(prefixer())
		.pipe(cssmin())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css));
});

gulp.task('images:build', function () {
	gulp.src(path.src.img) 
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()],
			interlaced: true
		}))
		.pipe(gulp.dest(path.build.img));
});


gulp.task('build', [
	'js:build',
	'styles:build',
	'images:build'
]);


gulp.task('watch', function(){
	gulp.watch([path.watch.style], function(event, cb) {
		gulp.start('styles:build');
	});
	gulp.watch([path.watch.js], function(event, cb) {
		gulp.start('js:build');
	});
	gulp.watch([path.watch.img], function(event, cb) {
		gulp.start('images:build');
	});
});


gulp.task('default', ['build', 'watch']);