<?
session_start();
$_SESSION['get'] = $_GET;
?>
<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>SAP Ariba — глобальная площадка для закупок и продаж</title>
		<meta name="description" content="SAP Ariba — международная площадка для проведения закупок, обладающая широким функционалом для оптимизации всех процессов закупок и продаж">
		<meta name="keywords" content="SAP Ariba САП Ариба закупки продажи закупочная деятельность зарубежные закупки бизнес-сеть">    
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="SAP Ariba" />
	<meta property="og:image" content="http://sap-crm.ru/images/logo-sap.png"/>
	<meta property="og:description" content="глобальная площадка для закупок и продаж" />
	<meta property="og:locale" content="ru_RU" />
		<link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="images/favicons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="images/favicons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">
		<link rel="manifest" href="manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="images/favicons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<link rel="stylesheet" href="css/app.css">

	</head>
	<!-- end Head -->

	<body>
		<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
			your browser</a> to improve your experience.</p>
		<![endif]-->

		<div class="layout">

			<!-- Promo -->
			<section class="head-section" id="product" style="background-image: url(images/head-section-bg04.jpg)">

				<!-- Header -->
				<header>
					<div class="container">
						<a href="#" class="logo" title="Sap">
							<img src="images/logo-sap.png" alt="Sap">
						</a>
						<button class="nav-toggle visible-xs" type="button" data-toggle="collapse" data-target="#main-nav" aria-expanded="false" aria-controls="main-nav">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="#" class="btn btn-sm btn-default-inverse demo hidden-xs" data-toggle="modal" data-target="#demo-modal">Демо бесплатно</a>
						<div class="head-contacts">
							<div class="holder">
								<strong class="block">+7 499 272-65-47</strong>
								<a class="link-underline" href="#" data-toggle="modal" data-target="#feedback-modal">Обратный звонок</a>
							</div>
						</div>
						<ul class="main-nav" id="main-nav">
							<li><a href="#ariba">Ariba</a></li>
							<li><a href="#about-sap">о SAP</a></li>
							<li><a href="#functions">функциональность</a></li>
              <li><a href="#results">результаты внедрения</a></li>
              <li><a href="#facts">характеристики</a></li>
							<li><a href="#order">заявка</a></li>
						</ul>
					</div>
				</header>
				<!-- end Header -->

				<div class="promo">
					<div class="container">
						<div class="row">

							<div class="col-md-6 col-md-offset-6">
								<div class="black-box-holder">
									<strong class="head black-box">
										SAP Ariba — международная площадка для закупок, объединяющая более 1,7 млн. поставщиков по всему миру и предоставляющая весь необходимый функционал для оптимизации и автоматизации закупочных процессов.
									</strong>
									<div class="sub-head">
										<strong>Разместите свой первый заказ уже сегодня.</strong>
									</div>
									<form action="#" class="j-feedback-form-1" data-form-id="1">
	                <input type="hidden" name="theme" value="SAP Ariba Demo">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<input class="form-control" type="text" placeholder="Компания" name="company" required />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<input class="form-control" type="email" placeholder="E-mail" name="email" required />
												</div>
											</div>
											<div class="clearfix hidden-xs"></div>
											<div class="col-sm-6">
												<div class="form-group">
													<input class="form-control" type="text" placeholder="Ваше имя" name="name" required />	
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<input class="form-control" type="text" placeholder="Телефон" name="phone" required />	
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-primary">Посмотреть демо</button>
	                  <span class="form-result"></span>
									</form>
								</div>
							</div>



						</div>
					</div>
				</div>
			</section>
			<!-- end Promo -->

			<section class="content-box" id="ariba">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="circle-ico-box">
								<div class="ico-block">
									<div class="cell">
										<img src="images/ico04.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Всемирная электронно-торговая площадка</strong>
									Единая точка доступа к огромному количеству поставщиков и закупщиков <br>
									<a href="#" data-toggle="modal" data-target="#load-presentation-modal">Посмотреть инфографику</a>
								</div>
							</div>
						</div>
            
						<div class="col-md-4">
							<div class="circle-ico-box">
								<div class="ico-block">
									<div class="cell">
										<img src="images/ico05.png" alt="">
									</div>
								</div>
								<div class="text">
                  <strong>Настоящее и будущее в решениях <br>SAP Ariba</strong>
									Как бизнес сеть может расширить возможности традиционных закупок?<br>
									<a href="#" data-toggle="modal" data-target="#load-presentation-modal">Посмотреть презентацию</a>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="circle-ico-box">
								<div class="ico-block">
									<div class="cell">
										<img src="images/ico06.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Внутренняя инфраструктура</strong>
									Инструменты для выбора поставщиков и управления заказами и каталогами, мощный финансовый блок для оптимизации оборотного капитала<br>
									<a href="#" data-toggle="modal" data-target="#load-presentation-modal">Прочитать брошюру</a>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</section>


			<!-- Statisticks -->
			<section class="statisticks" id="about-sap">
				<div class="container">
					<h2 class="brand-title">SAP Ariba в России и в мире</h2>

					<ul class="j-statisticks-gallery statisticks-gallery">
						<li>
							<div class="row">
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>1,7</strong> млн.
										</div>
										подключенных компаний <br>по всему миру
									</div>
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
                  					<div class="statistick-box">
										<div class="number">
											<strong>40</strong> млн.
										</div>
										заказов ежегодно<br>в бизнес-сети Ariba
									</div>
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>1200</strong>
										</div>
										новых поставщиков <br>в неделю
									</div>
								</div>                
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>190</strong>
										</div>
										стран — клиенты<br>по всему миру
									</div>
								</div>
							</div>
							<div class="text">Лучшие инструменты для внутренних и международных закупок и поставок доступны в SAP Ariba!</div>
						</li>
						<li>
							<div class="row">
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>80</strong> %
										</div>
										клиентов из сегмента <br>среднего и малого <br>бизнеса
									</div>									
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>87</strong> %
										</div>
										рейтинга крупнейших компаний <br>мира — клиенты SAP <br>(исследование Forbes Global)
									</div>
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>87</strong> %
										</div>
										26 компаний из топ-30 <br>рейтинга «Эксперт-400» — <br>клиенты SAP в России
									</div>
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>23</strong> года
										</div>
										компания SAP работает в России <br>(с решениями для крупного бизнеса)
									</div>
								</div>
							</div>
							<div class="text">Лучшие инструменты для внутренних и международных закупок и поставок доступны в SAP Ariba!</div>
						</li>
            
						<li>
							<div class="row">
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>1,2</strong> млн
										</div>
										пользователей CRM- и <br>ERP-системами SAP <br>в России и СНГ
									</div>
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>78</strong> %
										</div>
										выручки 400 крупнейших <br>предприятий России <br>производится клиентами SAP
									</div>
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>3.500</strong>
										</div>
										более 3.500 компаний <br>в России пользуются SAP
									</div>
								</div>
								<div class="col-md-3 col-xs-6 xm-block">
									<div class="statistick-box">
										<div class="number">
											<strong>87</strong> %
										</div>
										клиентов из сегмента <br>среднего и малого бизнеса
									</div>
								</div>
							</div>
							<div class="text">Лучшие инструменты для внутренних и международных закупок и поставок доступны в SAP Ariba!</div>
						</li>
					</ul>


				</div>
			</section>
			<!-- end Statisticks -->

			<div class="sep sep-brown"></div>

			<!-- Feedback Warning -->
			<section class="feedback-form-inline section-primary">
				<div class="container">
					<div class="form-head">Оставьте заявку и мы проведем для вас бесплатную демонстрацию</div>
					<form action="#" class="j-feedback-form-2" data-form-id="2">
          <input type="hidden" name="theme" value="Бесплатная демонстрация">
						<div class="row">
							<div class="col-lg-10">
								<div class="row row-small">
									<div class="col-lg-3 col-md-6">
										<div class="form-group">
											<input class="form-control" type="text" placeholder="Ваше имя" name="name" required />	
										</div>
									</div>
									<div class="col-lg-3 col-md-6">
										<div class="form-group">
											<input class="form-control" type="text" placeholder="Компания" name="company" required />	
										</div>
									</div>               
									<div class="col-lg-3 col-md-6">
										<div class="form-group">
											<input class="form-control" type="text" placeholder="Телефон" name="phone" required />	
										</div>
									</div>
									<div class="col-lg-3 col-md-6">
										<div class="form-group">
											<input class="form-control" type="email" placeholder="E-mail" name="email" required />	
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-2">
								<button type="submit" class="btn btn-primary">Получить демо</button>
                <span class="form-result"></span>
							</div>
						</div>
					</form>
				</div>
			</section>
			<!-- end Feedback Warning -->

			<!-- Screens Panel -->
			<section class="screens-panel-section" id="functions">
				<div class="container">
					<div class="head">
						<h2 class="brand-title brand-title-warning">Функциональность Ariba</h2>
						<div class="sub-title">
							SAP Ariba — это бизнес-сеть, которая предоставляет вам широчайшие функциональные возможности для поиска, выбора и работы с поставщиками и закупщиками. 
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-push-6">
							<div class="screen">
								<img src="images/functionality-img-01.png" alt="">
								<a href="#" class="show-screens show-screens-1" data-toggle="modal" data-target="#screen-modal-01">Посмотреть скриншоты</a>

								<div class="modal fade screen-modal" id="screen-modal-01" tabindex="-1" role="dialog" aria-labelledby="screen-modal-01">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Закупки</h4>
											<div class="j-screen-slider screen-slider owl-carousel">
												<div>
													<div class="slide-title">Рабочий стол</div>
													<img src="images/1-1.png">
												</div>
												<div>
													<div class="slide-title">Рабочий стол</div>
													<img src="images/1-1.png">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-md-pull-6">
							<div class="text">
								<h3>Закупки</h3>
								<ul class="list" style="font-size:17px;">
									<li>Работа с данными (Spend Visibility™) — интеграция, классификация, анализ;</li>
									<li>Поиск самых выгодных условий поставки и выбор поставщика на основе уникальных запатентованных алгоритмов (Sourcing™);</li>
									<li>Работа с контрактами (Contract Management™) — стандартизация и ускорение процесса;</li>
									<li>Создание каталогов и управление ими (Procurement Content);</li> 
									<li>Взаимоотношения с поставщиками (Supplier Management) и с бизнес-сообществом Ariba (Procure-to-Pay™);</li>
                  <li>Контроль цепочек поставок (Collaborative Supply Chain™).</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="sep"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="screen">
								<img src="images/functionality-img-02.png" alt="">
								<a href="#" class="show-screens show-screens-2" data-toggle="modal" data-target="#screen-modal-02">Посмотреть скриншоты</a>

								<div class="modal fade screen-modal" id="screen-modal-02" tabindex="-1" role="dialog" aria-labelledby="screen-modal-02">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Продажи</h4>
											<div class="j-screen-slider screen-slider owl-carousel">
												<div>
													<div class="slide-title">Пример отчётов для руководителя службы закупок</div>
													<img src="images/2-1.jpg">
												</div>
												<div>
													<div class="slide-title">Пример отчётов для руководителя службы закупок</div>
													<img src="images/2-1.jpg">
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<div class="text">
								<h3>Продажи</h3>
								<ul class="list" style="font-size:17px;">
									<li>Получите доступ на местные и глобальные рынки (Ariba Discovery™);</li>
									<li>Программы маркетинга и продаж Ariba — используйте все возможности продвижения в бизнес-сети Ariba;</li>
									<li>Решения для совместного управления заказами и каталогами Ariba Order and Catalog Collaboration.</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="sep"></div>
					<div class="row">
						<div class="col-md-6 col-md-push-6">
							<div class="screen">
								<img src="images/functionality-img-03.png" alt="">
								<a href="#" class="show-screens show-screens-3" data-toggle="modal" data-target="#screen-modal-03">Посмотреть скриншоты</a>

								<div class="modal fade screen-modal" id="screen-modal-03" tabindex="-1" role="dialog" aria-labelledby="screen-modal-03">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Управление денежными средствами</h4>
											<div class="j-screen-slider screen-slider owl-carousel">
												<div>
													<div class="slide-title">Учет средств при выборе поставщиков</div>
													<img src="images/3-1.png">
												</div>
												<div>
													<div class="slide-title">Учет средств при выборе поставщиков</div>
													<img src="images/3-1.png">
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-6 col-md-pull-6">
							<div class="text">
								<h3>Управление денежными средствами</h3>
								<ul class="list" style="font-size:17px;">
									<li>Закрытие  краткосрочных потребностей Ваших поставщиков в денежных средствах (Discount Professional);</li>
									<li>Финансирование дебиторской задолженности с помощью Ariba Receivables Financing;</li>
									<li>Интеллектуальное решение по выставлению счетов и контролю за ними (Invoice Management™);</li>
                  <li>Решение в области электронных платежей, упрощающее и оптимизирующее взаиморасчеты (Payment Management™).</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- end Screens Panel -->

			<section class="feedback-form-inline section-primary load-presentation">
				<div class="container">
					<strong>Это лишь часть функционала. Хотите оценить все возможности?</strong>
					<a href="" class="btn btn-primary"  data-toggle="modal" data-target="#load-presentation-modal">Cкачать презентацию </a>
				</div>
			</section>


			<!-- Similar -->
			<section class="silimar" id="results">
				<div class="container">
					<div class="row">
						<div class="col-md-7">
							<h2 class="brand-title">Результаты использования клиентами решений Ariba</h2>
							<h4>Все компании, использующие Ariba отмечают возросший уровень взаимодействия с контрагентами</h4>
							<div class="list-holder">
								<div class="row">
									<div class="col-sm-6">
										<ul class="list">
											<li>более чем на 50% снижение ошибок в документах закупки</li>
											<li>на 15% и более снижение стоимости закупок</li>
											<li>на 70% сокращение операционных затрат на процесс закупки</li>
										</ul>
									</div>
									<div class="col-sm-6">
										<ul class="list">
											<li>на 63% повышается маркетинговая эффективность</li>
											<li>на 20% сокращается цикл закупки</li>
											<li>на 10% увеличивается число потенциальных поставщиков</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="btn-row">
								<!--a href="#" class="btn btn-primary btn-sm xm-block" data-toggle="modal" data-target="#demo-sap-modal">Разместите свой первый заказ</a-->
								<a href="#" class="btn btn-primary btn-sm xm-block" data-toggle="modal" data-target="#load-presentation-modal">Разместите свой первый заказ</a>

								<!-- Demo Sap Modal -->
								<div class="modal fade feedback-modal" id="demo-sap-modal" tabindex="-1" role="dialog" aria-labelledby="demo-sap-modal">
									<div class="modal-dialog modal-sm" role="document">
										<div class="modal-content">
											<h4 class="modal-title" id="myModalLabel">Заказ</h4>
											<form action="#" class="j-demo-sap-form-modal hybris j-modal-form" data-form-id="3">
						          <input type="hidden" name="theme" value="Заказ">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												</div>
												<div class="modal-body">
													<div class="form-group">
														<input class="form-control form-control-inverse" type="text" placeholder="Компания" name="company" required />	
													</div>
													<div class="form-group">
														<input class="form-control form-control-inverse" type="email" placeholder="E-mail" name="email" required />	
													</div>
													<div class="form-group">
														<input class="form-control form-control-inverse" type="text" placeholder="Ваше имя" name="name" required />	
													</div>
													<div class="form-group">
														<input class="form-control form-control-inverse" placeholder="Телефон" name="phone" required />	
													</div>
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-primary">Отправить</button>
												</div>
												<h4 class="form-result"></h4>
											</form>
										</div>
									</div>
								</div>
								<!-- end Demo Sap Modal -->

							</div>
						</div>
						<div class="col-md-5">
							<div class="visual visual-hand">
								<img src="images/hand.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- end Similar -->



			<!-- Benefits -->
			<section class="benefits" id="facts">
				<div class="container">
					<h2 class="brand-title brand-title-warning">Ключевые характеристики</h2>
					<div class="row">
						<div class="col-lg-3 col-xs-6 xm-block">
							<div class="circle-ico-box">
								<div class="ico">
									<div class="cell">
										<img src="images/orange-ico01.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Глобализация</strong>
									SAP Ariba – система по повышению эффективности закупок №1. Работает в 190 странах мира и является стандартом для служб закупок огромного числа компаний.
								</div>
							</div>
						</div>
            
						<div class="col-lg-3 col-xs-6 xm-block">
							<div class="circle-ico-box">
								<div class="ico">
									<div class="cell">
										<img src="images/orange-ico02.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Локализация</strong>
									Центр обработки данных расположен на территории РФ, наличие сертификатов ФСТЭК и ФСБ.
								</div>
							</div>
						</div>
						<div class="clearfix visible-sm visible-xs"></div>
						<div class="col-lg-3 col-xs-6 xm-block">
							<div class="circle-ico-box">
								<div class="ico">
									<div class="cell">
										<img src="images/orange-ico03.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Открытость платформы </strong>
									Ariba позволяет объединять различные информационные системы. 
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-xs-6 xm-block">
							<div class="circle-ico-box">
								<div class="ico">
									<div class="cell">
										<img src="images/orange-ico04.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Глобальная поддержка </strong>
									Глобальная поддержка всех пользователей системы 24x7 по всему миру
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-3 col-xs-6 xm-block">
							<div class="circle-ico-box">
								<div class="ico">
									<div class="cell">
										<img src="images/orange-ico05.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Функциональность</strong>
									Система поддерживает все необходимые функции для процесса закупок.
								</div>
							</div>
						</div>
            
						<div class="col-lg-3 col-xs-6 xm-block">
							<div class="circle-ico-box">
								<div class="ico">
									<div class="cell">
										<img src="images/orange-ico06.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Быстрое развертывание</strong>
									Быстрое развертывания для полноценного использования в кратчайшие сроки.
								</div>
							</div>
						</div>
						<div class="clearfix visible-sm visible-xs"></div>
						<div class="col-lg-3 col-xs-6 xm-block">
							<div class="circle-ico-box">
								<div class="ico">
									<div class="cell">
										<img src="images/orange-ico07.png" alt="">
									</div>
								</div>
								<div class="text">
									<strong>Сервис</strong>
									Бизнес-консалтинг в проведении электронных закупочных процедур в разных странах, работа на более чем 20-ти языках по всему миру.
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- end Benefits -->


			<!-- Similar -->
			<section class="order section-primary" id="order">
				<div class="container">
					
					<div class="row dbl-row">
						<div class="col-lg-6">
							<h3 class="heading">Оставьте заявку</h3>
							<form action="#" class="j-feedback-form-order" data-form-id="4">
              <input type="hidden" name="theme" value="Заявка">
								<div class="row half-row">
									<div class="col-sm-6">
										<div class="form-group">
											<input class="form-control" type="text" placeholder="Компания" name="company" required />	
										</div>
										<div class="form-group">
											<input class="form-control" type="email" placeholder="E-mail" name="email" required />	
										</div>
										<div class="form-group">
											<input class="form-control" type="text" placeholder="Ваше имя" name="name" required />	
										</div>
										<div class="form-group">
											<input class="form-control" placeholder="Телефон" name="phone" required />	
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<textarea class="form-control" type="text" placeholder="Расскажите подробнее о своих процессах и мы покажем все возможности системы" name="text"></textarea>	
										</div>
									</div>
								</div>
								<button type="submit" class="btn btn-primary">Отправить заявку</button>
								<span class="btn-text">Перезвоним <br>в течение 15 минут</span>
                <span class="form-result"></span>
							</form>
						</div>
            
						<div class="col-lg-6" valign="top">
							<h3 class="heading">Или позвоните</h3>
							<div class="row">
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
											<strong class="user-head">Ваш персональный менеджер</strong>
											<div class="user-box">
												<div class="visual">
													<img src="images/user02.png" alt="">
												</div>
												<div class="info">
													<div class="phone">+7 499 272-65-47</div>
													<b>Артем Панов</b> <br>
													ведущий специалист <br>
													по работе с клиентами
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="text">
                    Опишите кратко свои процессы и мы покажем весь комплекс наших решений конкретно для вашего случая.
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</section>
			<!-- end Similar -->

		</div>

		<!-- Feedback Modal -->
		<div class="modal fade feedback-modal" id="feedback-modal" tabindex="-1" role="dialog" aria-labelledby="feedback-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<h4 class="modal-title" id="myModalLabel">Запросить обратный звонок</h4>
					<form action="#" class="j-feedback-form-modal j-modal-form" data-form-id="5">
          <input type="hidden" name="theme" value="Обратный звонок">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Ваше имя" name="name" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Телефон" name="phone" required />	
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Отправить</button>
						</div>
						<h4 class="form-result"></h4>
					</form>
				</div>
			</div>
		</div>
		<!-- end Feedback Modal -->

		<!-- Demo Modal -->
		<div class="modal fade feedback-modal" id="demo-modal" tabindex="-1" role="dialog" aria-labelledby="demo-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<h4 class="modal-title" id="myModalLabel">ОСТАВЬТЕ ЗАЯВКУ И МЫ ПРОВЕДЕМ ДЛЯ ВАС БЕСПЛАТНУЮ ДЕМОНСТРАЦИЮ ОНЛАЙН</h4>
					<form action="#" class="j-demo-form-modal j-modal-form" data-form-id="6">
          <input type="hidden" name="theme" value="Получить демо">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Компания" name="company" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" type="email" placeholder="E-mail" name="email" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Ваше имя" name="name" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" placeholder="Телефон" name="phone" required />	
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Отправить</button>
						</div>
						<h4 class="form-result"></h4>
					</form>
				</div>
			</div>
		</div>
		<!-- end Demo Modal -->

		<!-- Demo Sap Modal -->
		<div class="modal fade feedback-modal" id="load-presentation-modal" tabindex="-1" role="dialog" aria-labelledby="load-presentation-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<h4 class="modal-title" id="myModalLabel">Скачать материалы</h4>
					<form action="#" class="j-demo-sap-form-modal hybris j-modal-form" data-form-id="7">
          <input type="hidden" name="theme" value="Получить презентацию">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Компания" name="company" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" type="email" placeholder="E-mail" name="email" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Ваше имя" name="name" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" placeholder="Телефон" name="phone" required />	
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Отправить</button>
						</div>
						<h4 class="form-result"></h4>
					</form>
				</div>
			</div>
		</div>
		<!-- end Demo Sap Modal -->

		<!-- Demo Sap Modal -->
		<div class="modal fade feedback-modal demo-forrester-modal" id="demo-forrester-modal" tabindex="-1" role="dialog" aria-labelledby="demo-forrester-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<h4 class="modal-title" id="myModalLabel">Оставьте заявку и получите исследование Forester об эффективности внедрения SAP C4C</h4>
					<form action="#" class="j-demo-forrester-form-modal j-modal-form" data-form-id="8">
          <input type="hidden" name="theme" value="Получить исследование Forester">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Компания" name="company" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" type="email" placeholder="E-mail" name="email" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" type="text" placeholder="Ваше имя" name="name" required />	
							</div>
							<div class="form-group">
								<input class="form-control form-control-inverse" placeholder="Телефон" name="phone" required />	
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Отправить</button>
              <span class="form-result"></span>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- end Demo Sap Modal -->

		<!-- Video Modal -->
		<div class="modal fade video-modal" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="video-modal">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-content">
					<div class="embed-responsive embed-responsive-4by3 j-video-modal-content" style="padding-bottom: 56.3%;">
						<video id="video" class="embed-responsive-item" controls>
							<source src="media/sap.mp4" type="video/mp4">
							<source src="media/sap.webm" type="video/webm">
							<source src="media/sap.ogg" type="video/ogg">
							Your browser does not support the video tag.
						</video>
					</div>
				</div>
			</div>
		</div>
		<!-- end Video Modal -->


		<!-- Success Modal -->
		<div class="modal fade feedback-modal" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="success-modal">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<h4 class="form-result">Заявка успешно отправлена.<br>Наш специалист свяжется с Вами в ближайшее время.</h4>
				</div>
			</div>
		</div>
		<!-- end Demo Sap Modal -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold" />    
		<script src="js/app.js"></script>
		<script src="js/jquery.maskedinput.js"></script>
		<script src="js/form.js"></script>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35041520 = new Ya.Metrika({ id:35041520, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/35041520" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
	</body>
</html>