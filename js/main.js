function window_resize(){
  var page_h = $(window).height();
  $(".window").not(".document-workspace").height(page_h - $(".window").offset().top);
  $(".window.document-workspace").each(function(){
    $(this).height(page_h - $(this).offset().top - parseInt($(this).css("padding-top")) - parseInt($(this).css("padding-bottom")) );
  });  
}

  $(document).ready(function(){
    $(window).resize(window_resize);
    $(window).load(window_resize);
    $(".window").ready(window_resize);
    setInterval(window_resize, 1000);
  });