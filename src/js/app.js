/* Plugins */
//= ../vendor/jquery/jquery.min.js
//= ../vendor/jquery/jquery.validate.min.js
//= ../vendor/owl.carousel/owl.carousel.min.js
//= ../vendor/jquery.bxslider/jquery.bxslider.min.js
//= ../vendor/bootstrap-sass/javascripts/bootstrap/modal.js
//= ../vendor/bootstrap-sass/javascripts/bootstrap/collapse.js
//= ../vendor/bootstrap-sass/javascripts/bootstrap/transition.js
//= ../vendor/bootstrap-sass/javascripts/bootstrap/tab.js
//= ../vendor/fancybox/jquery.fancybox.js

$(function(){

	$('.j-feedback-form-modal').validate({});
	$('.j-demo-form-modal').validate();
	$('.j-demo-sup-form-modal').validate();
	$('.j-feedback-form-1').validate();
	$('.j-feedback-form-2').validate();
	$('.j-feedback-form-3').validate();
	$('.j-feedback-form-order').validate();
	$('.j-demo-forrester-form-modal').validate();

	$('.j-statisticks-gallery,.j-functionality-gallery,.j-testimonials-gallery').bxSlider({
		adaptiveHeight: true,
		pager: false
	});

	$('.j-screen-slider').owlCarousel({
	    loop: true,
	    margin: 0,
	    responsiveClass: true,
	    items: 1,
	    nav: true,
	    dots: false
	});

	/*$('.j-statisticks-gallery-1').owlCarousel({
	    loop: true,
	    margin: 0,
	    responsiveClass: true,
	    items: 1,
	    nav: true,
	    dots: false
	});

	$('.j-functionality-gallery').owlCarousel({
	    loop: true,
	    margin: 0,
	    responsiveClass: true,
	    items: 1,
	    nav: true,
	    dots: false
	});

	$('.j-testimonials-gallery').owlCarousel({
	    loop: true,
	    margin: 0,
	    responsiveClass: true,
	    items: 1,
	    nav: true,
	    dots: false
	});*/

	$('.main-nav a').on('click', function(e){
		e.preventDefault();
		$('.main-nav').removeClass('in');
		$('html,body').animate({
			scrollTop: $($(this).attr('href')).offset().top
		}, 'slow');
	});

	$('#video-modal').on('shown.bs.modal', function () {
		document.getElementById('video').play();
	});

	$('#video-modal').on('hidden.bs.modal', function () {
		document.getElementById('video').pause();
	});

	$(window).on('scroll', function(){

		if($(window).scrollTop() > 0) {
			$('header').addClass('fixed-header');
		} else {
			$('header').removeClass('fixed-header');
		}

	});

	$('.fancybox').fancybox({
		padding: 0,
		margin: [40,30,40,30],
		helpers: {
		    overlay: {
		      locked: false
		    }
		  }
	});

});